# Hello from docker

Run with the following command:


`docker run registry.gitlab.com/rwf-dev-public/hello-from-docker`


It will print "Hello from docker".This is a test image to try out gitlabs container registry

## With docker-compose

Save the following content into a file called `docker-compose.yml`.


```
version: "3.7"

services:
  hello-from-docker:
    image: registry.gitlab.com/rwf-dev-public/hello-from-docker
```

Then, in the directory where you created the file run `docker-compose up`.

  
