FROM ubuntu:focal

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["echo Hello from docker"]
